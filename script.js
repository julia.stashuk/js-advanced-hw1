//1.Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//У кожного об'єкту є прототип, він створюється автоматично. Якщо ми робимо об'єкт прототипом для іншого об'єкту, 
//то другий об'єкт отримує всі методі і властивості прототипу. Фактично, ми переносимо всю інформацію з прототипу в інший об'єкт-нащадок.
//Таким чином можна утворювати ланцюги об'ктів, копіюючи один об'єкт в інший.

//2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того, щоб передати аргументи класу-нащадку і він перейняв властивості та методи класу-батька.


class Employee {
constructor(name, age, salary){
    this.name = name;
    this.age = age;
    this.salary = salary;
}
get name(){
    return this.name;
}
set name(value) {
    this.name = value.trim().toLowerCase();
  }

get age(){
    return this.age;
}
set age(value) {
    this.age = value;
  }

get salary(){
    return this.salary;
}
set salary(value) {
    this.salary = value;
  }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
get lang(){
    return this.lang;
}
set lang(value) {
    this.lang = value.trim().split(" ");
  }

get salary(){
    return this.salary * 3;
}
set salary(value) {
    this.salary = value;
}
}


let user1 = new Programmer('Volodymyr', 23, 7500, 'Ukrainian');
let user2 = new Programmer('Tom', 27, 29500, 'English, German');
let user3 = new Programmer('Eva', 33, 44300, 'German');

console.log(user1);
console.log(user2);
console.log(user3);